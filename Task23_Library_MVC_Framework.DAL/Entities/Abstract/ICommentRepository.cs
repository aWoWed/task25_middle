﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Entities.Abstract
{
    public interface ICommentRepository : IRepository<Guid, Comment>
    {
        /// <summary>
        /// Gets comments by Contains(text) from Db
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Comments, which contains current text</returns>
        IQueryable<Comment> GetByContainsText(string text);

        /// <summary>
        /// Gets Async comments by ContainsText from Db
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Comments, which contains current text</returns>
        Task<IQueryable<Comment>> GetByContainsTextAsync(string text);
    }
}
