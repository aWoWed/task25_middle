﻿using System;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Entities.Abstract
{
    public interface ITagRepository : IRepository<Guid, Tag>
    { }
}
