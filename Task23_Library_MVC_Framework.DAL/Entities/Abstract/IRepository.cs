﻿using System.Linq;
using System.Threading.Tasks;

namespace Task23_Library_MVC_Framework.DAL.Entities.Abstract
{
    public interface IRepository<in TEntityKey, TEntity>
    {
        /// <summary>
        /// Gets All elems from Db
        /// </summary>
        IQueryable<TEntity> Get();

        /// <summary>
        /// Gets Async elems from Db
        /// </summary>
        /// <returns>Elems from Db</returns>
        Task<IQueryable<TEntity>> GetAsync();

        /// <summary>
        /// Gets elem by Key from Db
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Elem by key from Db</returns>
        TEntity GetByKey(TEntityKey key);

        /// <summary>
        /// Gets Async elem by Key from Db
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Elem by key from Db</returns>
        Task<TEntity> GetByKeyAsync(TEntityKey key);

        /// <summary>
        /// Gets elems by Name from Db
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Elem by Name</returns>
        IQueryable<TEntity> GetByName(string name);

        /// <summary>
        /// Gets elems Async by Name from Db
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Elems by Name</returns>
        Task<IQueryable<TEntity>> GetByNameAsync(string name);


        /// <summary>
        /// Inserts elem to Db
        /// </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary>
        /// Updates elem to Db
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes element by Key from Db
        /// </summary>
        /// <param name="key"></param>
        void DeleteByKey(TEntityKey key);

        /// <summary>
        /// Deletes All elems from Db
        /// </summary>
        void DeleteAll();
    }
}
