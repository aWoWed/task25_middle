﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Entities.Abstract
{
    public interface IArticleRepository : IRepository<Guid, Article>
    {
        /// <summary>
        /// Gets articles by Contains(text) from Db
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Articles, which contains current text</returns>
        IQueryable<Article> GetByContainsText(string text);

        /// <summary>
        /// Gets Async articles by ContainsText from Db
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Articles, which contains current text</returns>
        Task<IQueryable<Article>> GetByContainsTextAsync(string text);
    }
}
