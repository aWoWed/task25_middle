﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.DAL.Context;
using Task23_Library_MVC_Framework.DAL.Entities.Abstract;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Entities.EntityFramework
{
    public class TagRepository : ITagRepository
    {
        private readonly LibraryDbContext _appDbContext;

        public TagRepository(LibraryDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Gets All Tags from Db
        /// </summary>
        public IQueryable<Tag> Get() => _appDbContext.Tags;

        /// <summary>
        /// Gets Async All Tags from Db
        /// </summary>
        public Task<IQueryable<Tag>> GetAsync() => Task.FromResult(_appDbContext.Tags.AsQueryable());

        /// <summary>
        /// Gets Tag by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Tag with current key</returns>
        public Tag GetByKey(Guid key) => _appDbContext.Tags.FirstOrDefault(tag => tag.Id == key);

        /// <summary>
        /// Gets Async Tag by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Tag with current key</returns>
        public async Task<Tag> GetByKeyAsync(Guid key) => await _appDbContext.Tags.FirstOrDefaultAsync(tag => tag.Id == key);

        /// <summary>
        /// Gets Tag by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Tag with current name</returns>
        public IQueryable<Tag> GetByName(string name) => _appDbContext.Tags.Where(tag => tag.Name == name);

        /// <summary>
        /// Gets Async Tag by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Tag with current name</returns>
        public Task<IQueryable<Tag>> GetByNameAsync(string name) =>
            Task.FromResult(_appDbContext.Tags.Where(tag => tag.Name == name));

        /// <summary>
        /// Inserts Tag to Db
        /// </summary>
        /// <param name="entity"></param>
        public void Insert(Tag entity) => _appDbContext.Entry(entity).State = EntityState.Added;

        /// <summary>
        /// Updates Tag to Db
        /// </summary>
        /// <param name="entity"></param>
        public void Update(Tag entity)
        {
            _appDbContext.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Deletes Tags with current key
        /// </summary>
        /// <param name="key"></param>
        public void DeleteByKey(Guid key) => _appDbContext.Tags.Remove(new Tag {Id = key});

        /// <summary>
        /// Deletes all Tags
        /// </summary>
        public void DeleteAll() => _appDbContext.Tags.RemoveRange(_appDbContext.Tags);
    }
}
