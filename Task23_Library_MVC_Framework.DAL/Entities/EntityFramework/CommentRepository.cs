﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.DAL.Context;
using Task23_Library_MVC_Framework.DAL.Entities.Abstract;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Entities.EntityFramework
{
    public class CommentRepository : ICommentRepository
    {
        private readonly LibraryDbContext _appDbContext;

        public CommentRepository(LibraryDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Gets All Comments from Db
        /// </summary>
        public IQueryable<Comment> Get() => _appDbContext.Comments;

        /// <summary>
        /// Gets Async All Comments from Db
        /// </summary>
        public Task<IQueryable<Comment>> GetAsync() => Task.FromResult(_appDbContext.Comments.AsQueryable());

        /// <summary>
        /// Gets Comment by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Comment with current key</returns>
        public Comment GetByKey(Guid key) => _appDbContext.Comments.FirstOrDefault(comment => comment.Id == key);

        /// <summary>
        /// Gets Async Comment by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Comment with current key</returns>
        public async Task<Comment> GetByKeyAsync(Guid key) =>
            await _appDbContext.Comments.FirstOrDefaultAsync(comment => comment.Id == key);

        /// <summary>
        /// Gets Commentz by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Comments with current name</returns>
        public IQueryable<Comment> GetByName(string name) => _appDbContext.Comments.Where(comment => comment.Name == name);

        /// <summary>
        /// Gets Async Comments by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Comments with current name</returns>
        public Task<IQueryable<Comment>> GetByNameAsync(string name) =>
            Task.FromResult(_appDbContext.Comments.Where(comment => comment.Name == name));

        /// <summary>
        /// Gets Comments by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Comments with contains text</returns>
        public IQueryable<Comment> GetByContainsText(string text) =>
            _appDbContext.Comments.Where(comment => comment.Text.Contains(text));

        /// <summary>
        /// Gets Async Comments by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Comments with contains text</returns>
        public Task<IQueryable<Comment>> GetByContainsTextAsync(string text) => Task.FromResult(_appDbContext.Comments.Where(comment => comment.Text.ToLower().Contains(text.ToLower())));

        /// <summary>
        /// Inserts comment to Db
        /// </summary>
        /// <param name="entity"></param>
        public void Insert(Comment entity) => _appDbContext.Entry(entity).State = EntityState.Added;

        /// <summary>
        /// Updates comment to Db
        /// </summary>
        /// <param name="entity"></param>
        public void Update(Comment entity)
        {
            _appDbContext.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Deletes Comment with current key
        /// </summary>
        /// <param name="key"></param>
        public void DeleteByKey(Guid key) => _appDbContext.Comments.Remove(new Comment {Id = key});

        /// <summary>
        /// Deletes all Comments
        /// </summary>
        public void DeleteAll() => _appDbContext.Comments.RemoveRange(_appDbContext.Comments);
    }
}
