﻿namespace Task23_Library_MVC_Framework.DAL.Models
{
    public class FormAnswer : BaseModel
    {
        public string Genres { get; set; }
        public string ReadingPassion { get; set; }
    }
}
