﻿using System.Collections.Generic;

namespace Task23_Library_MVC_Framework.DAL.Models
{
    public class Article : BaseModel
    {
        public virtual ICollection<Tag> Tags { get; set; }

        public Article() => Tags = new List<Tag>();
        
    }
}
