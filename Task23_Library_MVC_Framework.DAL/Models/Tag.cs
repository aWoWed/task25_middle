﻿using System;
using System.Collections.Generic;

namespace Task23_Library_MVC_Framework.DAL.Models
{
    public class Tag
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Article> Articles { get; set; }

        public Tag()
        {
            Id = Guid.NewGuid();
            Articles = new List<Article>();
        } 
    }
}
