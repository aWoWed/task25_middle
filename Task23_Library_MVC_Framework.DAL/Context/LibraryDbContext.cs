﻿using System.Data.Entity;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Context
{
    public class LibraryDbContext : DbContext
    {
        /// <summary>
        /// Database Library Context
        /// </summary>
        static LibraryDbContext() => Database.SetInitializer(new LibraryContextInitializer());

        public LibraryDbContext() : base("name=DefaultConnection")
        { }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<FormAnswer> FormAnswers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
