﻿using System;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.DAL.Context;
using Task23_Library_MVC_Framework.DAL.Entities.Abstract;
using Task23_Library_MVC_Framework.DAL.Entities.EntityFramework;

namespace Task23_Library_MVC_Framework.DAL.UOfW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryDbContext _appDbContext;
        private readonly IArticleRepository _articleRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly IFormAnswerRepository _formAnswerRepository;
        private readonly ITagRepository _tagRepository;

        public UnitOfWork()
        {
            _appDbContext = new LibraryDbContext();
        }

        public IArticleRepository ArticleRepository => _articleRepository ?? new ArticleRepository(_appDbContext);

        public ITagRepository TagRepository => _tagRepository ?? new TagRepository(_appDbContext);

        public IFormAnswerRepository FormAnswerRepository =>
            _formAnswerRepository ?? new FormAnswerRepository(_appDbContext);

        public ICommentRepository CommentRepository => _commentRepository ?? new CommentRepository(_appDbContext);


        /// <summary>
        /// Save Changes in Db
        /// </summary>
        public void Save() => _appDbContext.SaveChanges();

        /// <summary>
        /// Save Changes Async in Db
        /// </summary>
        public async Task SaveAsync() => await _appDbContext.SaveChangesAsync();

        private bool _disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _appDbContext.Dispose();
                }

                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
