﻿using System;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.DAL.Entities.Abstract;

namespace Task23_Library_MVC_Framework.DAL.UOfW
{
    public interface IUnitOfWork : IDisposable
    {
        ICommentRepository CommentRepository { get; }
        IFormAnswerRepository FormAnswerRepository { get; }
        IArticleRepository ArticleRepository { get; }
        ITagRepository TagRepository { get; }

        /// <summary>
        /// Save Changes in Db
        /// </summary>
        void Save();

        /// <summary>
        /// Save Async Changes in Db
        /// </summary>
        Task SaveAsync();
    }
}
