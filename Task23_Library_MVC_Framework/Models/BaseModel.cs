﻿using System;

namespace Task23_Library_MVC_Framework.Models
{
    public abstract class BaseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public string Text { get; set; }

        protected BaseModel()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.Now;
        }
    }
}
