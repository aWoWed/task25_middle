﻿using System;
using System.Collections.Generic;
using Task23_Library_MVC_Framework.BLL.DTOs;

namespace Task23_Library_MVC_Framework.Models
{
    public class Tag
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ArticleDTO> Articles { get; set; }

        public Tag()
        {
            Id = Guid.NewGuid();
            Articles = new List<ArticleDTO>();
        }
    }
}
