﻿using System.Collections.Generic;
using Task23_Library_MVC_Framework.BLL.DTOs;

namespace Task23_Library_MVC_Framework.Models
{
    public class Article : BaseModel
    {
        public virtual ICollection<TagDTO> TagDtos { get; set; }

        public Article() => TagDtos = new List<TagDTO>();
    }
}
