﻿using System.Linq;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.EntityFramework;
using Task23_Library_MVC_Framework.Models;
using Task23_Library_MVC_Framework.ViewModels;

namespace Task23_Library_MVC_Framework.Controllers
{
    /// <summary>
    /// Comments page controller
    /// </summary>
    public class GuestController : Controller
    {
        private static readonly CommentService CommentService = new CommentService();
        /// <summary>
        /// Loads Comments from Db
        /// </summary>
        /// <returns>Comments Page</returns>
        public ActionResult Index(int page = 1, int pageSize = 2)
        {
            var commentsPerPage = CommentService.Get().AsQueryable().OrderBy(commentDto => commentDto.CreationDate).Skip((page - 1) * pageSize).Take(pageSize);
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = CommentService.Get().Count() };
            var commentViewModel = new CommentViewModel { PageInfo = pageInfo, CommentDtos = commentsPerPage };
            return View(commentViewModel);
        }

        /// <summary>
        /// Adds comment to Db
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns>Add comment</returns>
        [HttpPost]
        public ActionResult AddComment(CommentDTO comment, int page = 1, int pageSize = 2)
        {
            if (string.IsNullOrEmpty(comment.Name))
                ModelState.AddModelError("Name", "Error! Please enter your name!");

            else if (comment.Name.Length < 2)
                ModelState.AddModelError("Name", "Error! Your nickname is too short(> 2 symbols)!");

            if (string.IsNullOrEmpty(comment.Text))
                ModelState.AddModelError("Text", "Error! Please enter your text!");

            var commentsPerPage = CommentService.Get().AsQueryable().OrderBy(commentDto => commentDto.CreationDate).Skip((page - 1) * pageSize).Take(pageSize);
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = CommentService.Get().Count() };
            var commentViewModel = new CommentViewModel { PageInfo = pageInfo, CommentDtos = commentsPerPage };

            if (ModelState.IsValid)
            {
                CommentService.Insert(comment);
                return RedirectToAction("Index");
            }

            return View("Index", commentViewModel);
        }
    }
}
