﻿using System;
using System.Linq;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.EntityFramework;
using Task23_Library_MVC_Framework.Models;
using Task23_Library_MVC_Framework.ViewModels;

namespace Task23_Library_MVC_Framework.Controllers
{
    /// <summary>
    /// Articles page controller
    /// </summary>
    public class HomeController : Controller
    {
        private static readonly TagService TagService = new TagService();
        private static readonly ArticleService ArticleService = new ArticleService();
        /// <summary>
        /// Loads Articles from Db
        /// </summary>
        /// <returns>Articles Page</returns>
        [HttpGet]
        public ActionResult Index(int pageSize = 2, int page = 1)
        {
            var articlesPerPage = ArticleService.Get().AsQueryable().OrderBy(articleDto => articleDto.CreationDate)
                .Skip((page - 1) * pageSize).Take(pageSize);
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = ArticleService.Get().Count()};
            var articleViewModel = new ArticleViewModel { PageInfo = pageInfo, ArticlesDtos = articlesPerPage};
            return View(articleViewModel);
        }

        /// <summary>
        /// Loads Full article text
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Full article Page</returns>
        public ActionResult GetFullArticle(Guid id)
        {
            var articleDto = ArticleService.GetByKey(id);
            var article = new Article
            {
                Id = articleDto.Id,
                Name = articleDto.Name,
                Text = articleDto.Text,
                CreationDate = articleDto.CreationDate,
                TagDtos = articleDto.TagDtos
            };
            return View("FullArticle", article);
        }

        /// <summary>
        /// Loads Articles with current Tag from Db
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns>Articles with current </returns>
        public ActionResult ArticlesByTag(Guid id, string name, int pageSize = 2, int page = 1)
        {
            var articlesPerPage = ArticleService.Get()
                .Where(articleDto => articleDto.TagDtos.FirstOrDefault(tagDto => tagDto.Name == name).Id == id).AsQueryable()
                .OrderBy(articleDto => articleDto.CreationDate).Skip((page - 1) * pageSize).Take(pageSize);

            var pageInfo = new PageInfo
            {
                PageNumber = page, PageSize = pageSize,
                TotalItems = ArticleService.Get().Count(articleDto =>
                    articleDto.TagDtos.FirstOrDefault(tagDto => tagDto.Name == name).Id == id)
            };

            var articleViewModel = new ArticleViewModel { PageInfo = pageInfo, ArticlesDtos = articlesPerPage };
            return View(articleViewModel);
            
        }

        /// <summary>
        /// Add article default View
        /// </summary>
        /// <returns></returns>
        public ActionResult AddArticle()
        {
            return View();
        }

        /// <summary>
        /// Adds article to the main Page
        /// </summary>
        /// <param name="articleDto"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddArticle(ArticleDTO articleDto)
        {
            if(string.IsNullOrEmpty(articleDto.Name))
                ModelState.AddModelError("Name", "Error! Please enter your article's name");

            else if(articleDto.Name.Length < 2)
                ModelState.AddModelError("Name", "Error! Your article's name is too short(> 2 symbols)!");

            if (string.IsNullOrEmpty(articleDto.Text))
                ModelState.AddModelError("Text", "Error! Please enter your article's text!");

            else if (articleDto.Text.Length < 10)
                ModelState.AddModelError("Text", "Error! Your article's text is too short(> 10 symbols)!");

            if (ModelState.IsValid)
            {
                ArticleService.Insert(articleDto);
                return RedirectToAction("Index");
            }

            return View("AddArticle");
        }

        /// <summary>
        /// Adds tag to Db
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tag"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns>Add tag to Db</returns>
        [HttpPost]
        public ActionResult AddTag(Guid id, TagDTO tag, int pageSize = 2, int page = 1)
        {
            var articlesPerPage = ArticleService.Get().AsQueryable().OrderBy(articleDto => articleDto.CreationDate)
                .Skip((page - 1) * pageSize).Take(pageSize);

            var pageInfo = new PageInfo
                { PageNumber = page, PageSize = pageSize, TotalItems = ArticleService.Get().Count() };
            var articleViewModel = new ArticleViewModel { PageInfo = pageInfo, ArticlesDtos = articlesPerPage };

            if (string.IsNullOrEmpty(tag.Name))
                ModelState.AddModelError("Name", "Error! Please enter your tag name!");

            else if (tag.Name.Length < 2)
                ModelState.AddModelError("Name", "Error! Your tag name is too short(> 2 symbols)!");

            if (ModelState.IsValid)
            {
                var article = ArticleService.GetByKey(id);

                if (tag.ArticlesDtos.FirstOrDefault(articleDto => articleDto.Id == article.Id) == null)
                {
                    tag.ArticlesDtos.Add(article);
                }

                if (TagService.Get().FirstOrDefault(tagDto => tagDto.Name == tag.Name) == null)
                {
                    if (article.TagDtos.FirstOrDefault(tagDto => tagDto.Name == tag.Name) == null)
                    {
                        tag.Id = Guid.NewGuid();
                        article.TagDtos.Add(tag);
                    }
                }
                else
                {
                    if (tag.ArticlesDtos.FirstOrDefault(articleDto => articleDto.Name == article.Name) != null)
                    {
                        tag.Id = TagService.Get().FirstOrDefault(tagDto => tagDto.Name == tag.Name).Id;
                        article.TagDtos.Add(tag);
                    }
                }

                ArticleService.Update(article);

                return RedirectToAction("Index");
            }

            return View("Index", articleViewModel);
        }
    }
}