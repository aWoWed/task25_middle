﻿using System.Collections.Generic;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.EntityFramework;

namespace Task23_Library_MVC_Framework.Controllers
{
    /// <summary>
    /// Form page controller
    /// </summary>
    public class FormController : Controller
    {
        private static readonly FormAnswerService FormAnswerService = new FormAnswerService();

        /// <summary>
        /// Loads Form ViewModel
        /// </summary>
        /// <returns>Form ViewModel</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Adds FormAnswer To the db
        /// </summary>
        /// <param name="formAnswer"></param>
        /// <param name="genres"></param>
        /// <returns>FormAnswer Page</returns>
        [HttpPost]
        public ActionResult Result(FormAnswerDTO formAnswer, IList<string> genres)
        {
            if (string.IsNullOrEmpty(formAnswer.Name))
                ModelState.AddModelError("Name","Please enter your name");

            else if (formAnswer.Name.Length < 2)
                ModelState.AddModelError("Name", "Error! Your nickname is too short(> 2 symbols)!");

            if (string.IsNullOrEmpty(formAnswer.Genres) || genres == null)
                ModelState.AddModelError("Genres", "Select at least one of the genres");

            if (string.IsNullOrEmpty(formAnswer.ReadingPassion))
                ModelState.AddModelError("ReadingPassion", "Please choice your reading passion");

            if (genres != null)
                foreach (var genre in genres)
                {
                    if (!formAnswer.Genres.Contains(genre))
                        formAnswer.Genres += " " + genre;
                }

            if (!ModelState.IsValid) return View("Index");
            FormAnswerService.Insert(formAnswer);
            return RedirectToAction("Result");
        }

        /// <summary>
        /// Loads FormAnswer Page
        /// </summary>
        /// <returns>FormAnswer Page</returns>
        [HttpGet]
        public ActionResult Result()
        {
            return View("Result",  FormAnswerService.Get());
        }
    }
}
