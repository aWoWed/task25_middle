﻿using System.Linq;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.ViewModels
{
    public class CommentViewModel
    {

        public PageInfo PageInfo { get; set; }
        public IQueryable<CommentDTO> CommentDtos { get; set; }
    }
}
