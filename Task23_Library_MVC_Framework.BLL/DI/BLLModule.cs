﻿using Ninject.Modules;
using Task23_Library_MVC_Framework.DAL.UOfW;

namespace Task23_Library_MVC_Framework.BLL.DI
{
    /// <summary>
    /// Ninject module for registering needed dependencies for BLL
    /// </summary>

    public class BLLModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
