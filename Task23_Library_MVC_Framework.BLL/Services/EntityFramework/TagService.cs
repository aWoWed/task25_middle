﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Internal;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.Abstract;
using Task23_Library_MVC_Framework.DAL.Models;
using Task23_Library_MVC_Framework.DAL.UOfW;

namespace Task23_Library_MVC_Framework.BLL.Services.EntityFramework
{
    public class TagService : ITagService
    {
        private readonly UnitOfWork _unitOfWork;

        public TagService()
        {
            _unitOfWork = new UnitOfWork();
        }

        public IQueryable<TagDTO> Get() =>
            _unitOfWork.TagRepository.Get().Select(tag => new TagDTO
            {
                Id = tag.Id, Name = tag.Name,
                ArticlesDtos = tag.Articles.Select(article => new ArticleDTO
                    {
                        Id = article.Id, Name = article.Name, Text = article.Text, CreationDate = article.CreationDate
                    })
                    .ToList()
            });

        public Task<IQueryable<TagDTO>> GetAsync() => Task.FromResult(_unitOfWork.TagRepository.Get()
            .Select(tag => new TagDTO
            {
                Id = tag.Id, Name = tag.Name,
                ArticlesDtos = tag.Articles.Select(article => new ArticleDTO
                    {
                        Id = article.Id, Name = article.Name, Text = article.Text, CreationDate = article.CreationDate
                    })
                    .ToList()
            }));

        public TagDTO GetByKey(Guid key)
        {
            var tag = _unitOfWork.TagRepository.GetByKey(key);
            if(tag == null)
                throw new ArgumentException("Tag is not found");
            return new TagDTO
            {
                Id = tag.Id,
                Name = tag.Name,
                ArticlesDtos = tag.Articles.Select(article => new ArticleDTO
                    {
                        Id = article.Id, Name = article.Name, Text = article.Text, CreationDate = article.CreationDate
                    })
                    .ToList()
            };
        }

        public Task<TagDTO> GetByKeyAsync(Guid key)
        {

            var tag = _unitOfWork.TagRepository.GetByKey(key);

            if (tag == null)
                throw new ArgumentException("Tag is not found");

            return Task.FromResult(new TagDTO
            {
                Id = tag.Id,
                Name = tag.Name,
                ArticlesDtos = tag.Articles.Select(article => new ArticleDTO
                    {
                        Id = article.Id, Name = article.Name, Text = article.Text, CreationDate = article.CreationDate
                    })
                    .ToList()
            });
        }

        public IQueryable<TagDTO> GetByName(string name) => _unitOfWork.TagRepository.GetByName(name)
            .Select(tag => new TagDTO
            {
                Id = tag.Id, Name = tag.Name,
                ArticlesDtos = tag.Articles.Select(article => new ArticleDTO
                    {
                        Id = article.Id, Name = article.Name, Text = article.Text, CreationDate = article.CreationDate
                    })
                    .ToList()
            });

        public Task<IQueryable<TagDTO>> GetByNameAsync(string name) => Task.FromResult(_unitOfWork.TagRepository
            .GetByName(name)
            .Select(tag => new TagDTO
            {
                Id = tag.Id, Name = tag.Name,
                ArticlesDtos = tag.Articles.Select(article => new ArticleDTO
                    {
                        Id = article.Id, Name = article.Name, Text = article.Text, CreationDate = article.CreationDate
                    })
                    .ToList()
            }));

        public void Insert(TagDTO entity)
        {
            _unitOfWork.TagRepository.Insert(new Tag
            {
                Id = entity.Id,
                Name = entity.Name,
                Articles = entity.ArticlesDtos.Select(article => new Article
                    {
                        Id = article.Id, Name = article.Name, Text = article.Text, CreationDate = article.CreationDate
                    })
                    .ToList()
            });
            _unitOfWork.Save();
        }

        public void Update(TagDTO entity)
        {
            var tag = _unitOfWork.TagRepository.GetByKey(entity.Id);
            
            if(tag == null)
                return;

            tag.Name = entity.Name;

            entity.ArticlesDtos.ForAll(articleDto =>
            {
                var art = tag.Articles.FirstOrDefault(article => article.Id == articleDto.Id);
                var articleInArticlesByName = _unitOfWork.ArticleRepository.GetByName(articleDto.Name).FirstOrDefault();
                if (art == null && articleInArticlesByName == null)
                    tag.Articles.Add(new Article { Id = articleDto.Id, Name = articleDto.Name, Text = articleDto.Text, CreationDate = articleDto.CreationDate });
                else if (articleInArticlesByName != null && art == null)
                {
                    articleInArticlesByName.Id = articleDto.Id;
                    articleInArticlesByName.Name = articleDto.Name;
                    articleInArticlesByName.Text = articleDto.Text;
                    articleInArticlesByName.CreationDate = articleDto.CreationDate;
                    tag.Articles.Add(articleInArticlesByName);
                }
                else
                {
                    art.Id = articleDto.Id;
                    art.Name = articleDto.Name;
                    art.Text = articleDto.Text;
                    art.CreationDate = articleDto.CreationDate;
                }
            });

            _unitOfWork.TagRepository.Update(tag);
            _unitOfWork.Save();
        }

        public void DeleteByKey(Guid key)
        {
            _unitOfWork.TagRepository.DeleteByKey(key);
            _unitOfWork.Save();
        }

        public void DeleteAll()
        {
            _unitOfWork.TagRepository.DeleteAll();
            _unitOfWork.Save();
        }
    }
}
