﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.Abstract;
using Task23_Library_MVC_Framework.DAL.Models;
using Task23_Library_MVC_Framework.DAL.UOfW;

namespace Task23_Library_MVC_Framework.BLL.Services.EntityFramework
{
    public class CommentService : ICommentService
    {
        private readonly UnitOfWork _unitOfWork;

        public CommentService()
        {
            _unitOfWork = new UnitOfWork();
        }

        public IQueryable<CommentDTO> Get() => _unitOfWork.CommentRepository.Get().Select(comment => new CommentDTO
            {Id = comment.Id, Name = comment.Name, Text = comment.Text, CreationDate = comment.CreationDate});

        public Task<IQueryable<CommentDTO>> GetAsync() => Task.FromResult(_unitOfWork.CommentRepository.GetAsync()
            .Result.Select(comment => new CommentDTO
                {Id = comment.Id, Name = comment.Name, Text = comment.Text, CreationDate = comment.CreationDate}));

        public CommentDTO GetByKey(Guid key)
        {
            var comment = _unitOfWork.CommentRepository.GetByKey(key);
            if(comment == null)
                throw new ArgumentException("Comment not found");
            return new CommentDTO
            {
                Id = comment.Id,
                Name = comment.Name,
                Text = comment.Text,
                CreationDate = comment.CreationDate
            };
        }

        public Task<CommentDTO> GetByKeyAsync(Guid key)
        {
            var comment = _unitOfWork.CommentRepository.GetByKeyAsync(key).Result;
            if (comment == null)
                throw new ArgumentException("Comment not found");
            return Task.FromResult(new CommentDTO
            {
                Id = comment.Id,
                Name = comment.Name,
                Text = comment.Text,
                CreationDate = comment.CreationDate
            });
        }

        public IQueryable<CommentDTO> GetByName(string name) => _unitOfWork.CommentRepository.GetByName(name)
            .Select(comment => new CommentDTO
                {Id = comment.Id, Text = comment.Text, Name = comment.Name, CreationDate = comment.CreationDate});

        public Task<IQueryable<CommentDTO>> GetByNameAsync(string name) => Task.FromResult(_unitOfWork.CommentRepository
            .GetByNameAsync(name).Result.Select(comment => new CommentDTO
                {Id = comment.Id, Text = comment.Text, Name = comment.Name, CreationDate = comment.CreationDate}));

        public IQueryable<CommentDTO> GetByContainsText(string text) => _unitOfWork.CommentRepository
            .GetByContainsText(text).Select(comment => new CommentDTO
                {Id = comment.Id, Text = comment.Text, Name = comment.Name, CreationDate = comment.CreationDate});

        public Task<IQueryable<CommentDTO>> GetByContainsTextAsync(string text) => Task.FromResult(_unitOfWork
            .CommentRepository.GetByContainsTextAsync(text).Result.Select(comment => new CommentDTO
                {Id = comment.Id, Text = comment.Text, Name = comment.Name, CreationDate = comment.CreationDate}));

        public void Insert(CommentDTO entity)
        {
            _unitOfWork.CommentRepository.Insert(new Comment
            {
                Id = entity.Id,
                Name = entity.Name,
                Text = entity.Text,
                CreationDate = entity.CreationDate
            });
            _unitOfWork.Save();
        }

        public void Update(CommentDTO entity)
        {
            _unitOfWork.CommentRepository.Update(new Comment
            {
                Id = entity.Id,
                Name = entity.Name,
                Text = entity.Text,
                CreationDate = entity.CreationDate
            });
            _unitOfWork.Save();
        }

        public void DeleteByKey(Guid key)
        {
            _unitOfWork.CommentRepository.DeleteByKey(key);
            _unitOfWork.Save();
        }

        public void DeleteAll()
        {
            _unitOfWork.ArticleRepository.DeleteAll();
            _unitOfWork.Save();
        }
    }
}
