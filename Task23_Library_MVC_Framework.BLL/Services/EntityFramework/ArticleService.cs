﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Internal;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.Abstract;
using Task23_Library_MVC_Framework.DAL.Models;
using Task23_Library_MVC_Framework.DAL.UOfW;

namespace Task23_Library_MVC_Framework.BLL.Services.EntityFramework
{
    public class ArticleService : IArticleService
    {
        private readonly UnitOfWork _unitOfWork;

        public ArticleService()
        { 
            _unitOfWork = new UnitOfWork();
        }

        public IQueryable<ArticleDTO> Get() => _unitOfWork.ArticleRepository.Get().Select(article => new ArticleDTO
        {
            Id = article.Id, Name = article.Name, CreationDate = article.CreationDate, Text = article.Text,
            TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
        });

        public Task<IQueryable<ArticleDTO>> GetAsync() => Task.FromResult(_unitOfWork.ArticleRepository.GetAsync()
            .Result.Select(article => new ArticleDTO
            {
                Id = article.Id, Name = article.Name, CreationDate = article.CreationDate, Text = article.Text,
                TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
            }));

        public ArticleDTO GetByKey(Guid key)
        {
            var article = _unitOfWork.ArticleRepository.GetByKey(key);
            if (article == null)
                throw new ArgumentException("Article is not found!");

            return new ArticleDTO
            {
                Id = article.Id,
                Name = article.Name,
                CreationDate = article.CreationDate,
                Text = article.Text,
                TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
            };
        }

        public Task<ArticleDTO> GetByKeyAsync(Guid key)
        {
            var article = _unitOfWork.ArticleRepository.GetByKeyAsync(key).Result;
            if (article == null)
                throw new ArgumentException("Article is not found!");
            return Task.FromResult(new ArticleDTO
            {
                Id = article.Id,
                Name = article.Name,
                CreationDate = article.CreationDate,
                Text = article.Text,
                TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
            });
        }

        public IQueryable<ArticleDTO> GetByName(string name) => _unitOfWork.ArticleRepository.GetByName(name).Select(
            article => new ArticleDTO
            {
                Id = article.Id, Name = article.Name, CreationDate = article.CreationDate, Text = article.Text,
                TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
            });

        public Task<IQueryable<ArticleDTO>> GetByNameAsync(string name) => Task.FromResult(_unitOfWork.ArticleRepository
            .GetByName(name).Select(article => new ArticleDTO
            {
                Id = article.Id, Name = article.Name, CreationDate = article.CreationDate, Text = article.Text,
                TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
            }));

        public IQueryable<ArticleDTO> GetByContainsText(string text) => _unitOfWork.ArticleRepository
            .GetByContainsText(text).Select(article => new ArticleDTO
            {
                Id = article.Id, Name = article.Name, CreationDate = article.CreationDate, Text = article.Text,
                TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
            });

        public Task<IQueryable<ArticleDTO>> GetByContainsTextAsync(string text) => Task.FromResult(_unitOfWork
            .ArticleRepository.GetByContainsTextAsync(text).Result.Select(article => new ArticleDTO
            {
                Id = article.Id, Name = article.Name, CreationDate = article.CreationDate, Text = article.Text,
                TagDtos = article.Tags.Select(tag => new TagDTO {Id = tag.Id, Name = tag.Name}).ToList()
            }));

        public void Insert(ArticleDTO entity)
        {
            _unitOfWork.ArticleRepository.Insert(new Article
            {
                Id = entity.Id,
                Name = entity.Name,
                CreationDate = entity.CreationDate,
                Text = entity.Text,
                Tags = entity.TagDtos.Select(tag => new Tag { Id = tag.Id, Name = tag.Name }).ToList()
            });
            _unitOfWork.Save();
        }

        public void Update(ArticleDTO entity)
        {
            var article = _unitOfWork.ArticleRepository.GetByKey(entity.Id);

            if (article == null)
                return;

            article.CreationDate = entity.CreationDate;
            article.Name = entity.Name;
            article.Text = entity.Text;

            entity.TagDtos.ForAll(tagDto =>
            {
                var tg = article.Tags.FirstOrDefault(tag => tag.Id == tagDto.Id);
                var tagInTagsByName = _unitOfWork.TagRepository.GetByName(tagDto.Name).FirstOrDefault();
                if (tg == null && tagInTagsByName == null)
                    article.Tags.Add(new Tag {Id = tagDto.Id, Name = tagDto.Name});
                else if (tagInTagsByName != null && tg == null)
                {
                    tagInTagsByName.Id = tagDto.Id;
                    tagInTagsByName.Name = tagDto.Name;
                    article.Tags.Add(tagInTagsByName);
                }
                else
                {
                    tg.Id = tagDto.Id;
                    tg.Name = tagDto.Name;
                }
            });

            _unitOfWork.ArticleRepository.Update(article);
            _unitOfWork.Save();
        }

        public void DeleteByKey(Guid key)
        {
            _unitOfWork.ArticleRepository.DeleteByKey(key);
            _unitOfWork.Save();
        }

        public void DeleteAll()
        {
            _unitOfWork.ArticleRepository.DeleteAll();
            _unitOfWork.Save();
        }
    }
}
