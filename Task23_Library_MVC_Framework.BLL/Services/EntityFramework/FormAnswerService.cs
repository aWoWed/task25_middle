﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.BLL.DTOs;
using Task23_Library_MVC_Framework.BLL.Services.Abstract;
using Task23_Library_MVC_Framework.DAL.Models;
using Task23_Library_MVC_Framework.DAL.UOfW;

namespace Task23_Library_MVC_Framework.BLL.Services.EntityFramework
{
    public class FormAnswerService : IFormAnswerService
    {
        private readonly UnitOfWork _unitOfWork;

        public FormAnswerService()
        {
            _unitOfWork = new UnitOfWork();
        }

        public IQueryable<FormAnswerDTO> Get() => _unitOfWork.FormAnswerRepository.Get().Select(formAnswer =>
            new FormAnswerDTO
            {
                Id = formAnswer.Id, Genres = formAnswer.Genres, Text = formAnswer.Text, Name = formAnswer.Name,
                CreationDate = formAnswer.CreationDate, ReadingPassion = formAnswer.ReadingPassion
            });

        public Task<IQueryable<FormAnswerDTO>> GetAsync() => Task.FromResult(_unitOfWork.FormAnswerRepository.GetAsync()
            .Result.Select(formAnswer =>
                new FormAnswerDTO
                {
                    Id = formAnswer.Id,
                    Genres = formAnswer.Genres,
                    Text = formAnswer.Text,
                    Name = formAnswer.Name,
                    CreationDate = formAnswer.CreationDate,
                    ReadingPassion = formAnswer.ReadingPassion
                }));

        public FormAnswerDTO GetByKey(Guid key)
        {
            var formAnswer = _unitOfWork.FormAnswerRepository.GetByKey(key);
            if (formAnswer == null)
                throw new ArgumentException("Form answer is not found");
            return new FormAnswerDTO
            {
                Id = formAnswer.Id,
                Genres = formAnswer.Genres,
                Text = formAnswer.Text,
                Name = formAnswer.Name,
                CreationDate = formAnswer.CreationDate,
                ReadingPassion = formAnswer.ReadingPassion
            };
        }

        public Task<FormAnswerDTO> GetByKeyAsync(Guid key)
        {
            var formAnswer = _unitOfWork.FormAnswerRepository.GetByKeyAsync(key).Result;
            if (formAnswer == null)
                throw new ArgumentException("Form answer is not found");
            return Task.FromResult(new FormAnswerDTO
            {
                Id = formAnswer.Id,
                Genres = formAnswer.Genres,
                Text = formAnswer.Text,
                Name = formAnswer.Name,
                CreationDate = formAnswer.CreationDate,
                ReadingPassion = formAnswer.ReadingPassion
            });
        }

        public IQueryable<FormAnswerDTO> GetByName(string name) => _unitOfWork.FormAnswerRepository.GetByName(name)
            .Select(formAnswer => new FormAnswerDTO
            {
                Id = formAnswer.Id,
                Genres = formAnswer.Genres,
                Text = formAnswer.Text,
                Name = formAnswer.Name,
                CreationDate = formAnswer.CreationDate,
                ReadingPassion = formAnswer.ReadingPassion
            });

        public Task<IQueryable<FormAnswerDTO>> GetByNameAsync(string name) => Task.FromResult(_unitOfWork
            .FormAnswerRepository.GetByName(name)
            .Select(formAnswer => new FormAnswerDTO
            {
                Id = formAnswer.Id,
                Genres = formAnswer.Genres,
                Text = formAnswer.Text,
                Name = formAnswer.Name,
                CreationDate = formAnswer.CreationDate,
                ReadingPassion = formAnswer.ReadingPassion
            }));

        public void Insert(FormAnswerDTO entity)
        {
            _unitOfWork.FormAnswerRepository.Insert(new FormAnswer
            {
                Id = entity.Id,
                Genres = entity.Genres,
                Text = entity.Text,
                Name = entity.Name,
                CreationDate = entity.CreationDate,
                ReadingPassion = entity.ReadingPassion
            });
            _unitOfWork.Save();
        }

        public void Update(FormAnswerDTO entity)
        {
            _unitOfWork.FormAnswerRepository.Update(new FormAnswer
            {
                Id = entity.Id,
                Genres = entity.Genres,
                Text = entity.Text,
                Name = entity.Name,
                CreationDate = entity.CreationDate,
                ReadingPassion = entity.ReadingPassion
            });
            _unitOfWork.Save();
        }

        public void DeleteByKey(Guid key)
        {
            _unitOfWork.FormAnswerRepository.DeleteByKey(key);
            _unitOfWork.Save();
        }

        public void DeleteAll()
        {
            _unitOfWork.FormAnswerRepository.DeleteAll();
            _unitOfWork.Save();
        }
    }
}
