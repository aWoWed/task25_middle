﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.BLL.DTOs;

namespace Task23_Library_MVC_Framework.BLL.Services.Abstract
{
    public interface ICommentService : IService<Guid, CommentDTO>
    {
        /// <summary>
        /// Get comments by Contains(text) from DTO
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Comments, which contains current text</returns>
        IQueryable<CommentDTO> GetByContainsText(string text);

        /// <summary>
        /// Gets Async comments by ContainsText from DTO
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Comments, which contains current text</returns>
        Task<IQueryable<CommentDTO>> GetByContainsTextAsync(string text);
    }
}
