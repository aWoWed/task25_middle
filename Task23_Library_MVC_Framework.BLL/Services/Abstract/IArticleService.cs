﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.BLL.DTOs;

namespace Task23_Library_MVC_Framework.BLL.Services.Abstract
{
    public interface IArticleService : IService<Guid, ArticleDTO>
    {
        /// <summary>
        /// Gets articles by Contains(text) from DTO
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Articles, which contains current text</returns>
        IQueryable<ArticleDTO> GetByContainsText(string text);

        /// <summary>
        /// Gets Async articles by ContainsText from DTO
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Articles, which contains current text</returns>
        Task<IQueryable<ArticleDTO>> GetByContainsTextAsync(string text);
    }
}
