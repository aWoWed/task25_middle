﻿using System.Linq;
using System.Threading.Tasks;

namespace Task23_Library_MVC_Framework.BLL.Services.Abstract
{
    public interface IService<in TEntityKey, TEntity>
    {
        /// <summary>
        /// Gets All elems from DTO
        /// </summary>
        IQueryable<TEntity> Get();

        /// <summary>
        /// Gets Async elems from DTO
        /// </summary>
        /// <returns>Elems from Db</returns>
        Task<IQueryable<TEntity>> GetAsync();

        /// <summary>
        /// Gets elem by Key from DTO
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Elem by key from Db</returns>
        TEntity GetByKey(TEntityKey key);

        /// <summary>
        /// Gets Async elem by Key from DTO
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Elem by key from Db</returns>
        Task<TEntity> GetByKeyAsync(TEntityKey key);

        /// <summary>
        /// Gets elems by Name from DTO
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Elem by Name</returns>
        IQueryable<TEntity> GetByName(string name);

        /// <summary>
        /// Gets elems Async by Name from DTO
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Elems by Name</returns>
        Task<IQueryable<TEntity>> GetByNameAsync(string name);

        /// <summary>
        /// Inserts elem to Db
        /// </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary>
        /// Updates elem to Db
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes element by Key from DTO
        /// </summary>
        /// <param name="key"></param>
        void DeleteByKey(TEntityKey key);

        /// <summary>
        /// Deletes All elems from DTO
        /// </summary>
        void DeleteAll();
    }
}
