﻿using System;
using Task23_Library_MVC_Framework.BLL.DTOs;

namespace Task23_Library_MVC_Framework.BLL.Services.Abstract
{
    public interface ITagService : IService<Guid, TagDTO>
    { }
}
