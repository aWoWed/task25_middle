﻿using System.Collections.Generic;

namespace Task23_Library_MVC_Framework.BLL.DTOs
{
    public class ArticleDTO : BaseDTO
    {
        public ICollection<TagDTO> TagDtos { get; set; }
        public ArticleDTO() => TagDtos = new List<TagDTO>();
    }
}
