﻿using System;
using System.Collections.Generic;

namespace Task23_Library_MVC_Framework.BLL.DTOs
{
    public class TagDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ArticleDTO> ArticlesDtos { get; set; }

        public TagDTO()
        {
            Id = Guid.NewGuid();
            ArticlesDtos = new List<ArticleDTO>();
        }
    }
}
