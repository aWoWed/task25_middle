﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.Controllers;

namespace Library_MVC_Framework.Tests
{
    [TestClass]
    public class LibraryTests
    {
        [TestMethod]
        public void HomeControllerTest_ReturnNotNullView()
        {
            var controller = new HomeController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GuestControllerTest_ReturnNotNullView()
        {
            var controller = new GuestController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void FormControllerTest_ReturnNotNullView()
        {
            var controller = new FormController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void HomeControllerReturnViewTest_IndexReturnCorrectViewName()
        {
            var controller = new HomeController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void GuestControllerReturnViewTest_IndexReturnCorrectViewName()
        {
            var controller = new GuestController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void FormControllerReturnViewTest_IndexReturnCorrectViewName()
        {
            var controller = new FormController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
    }
}
